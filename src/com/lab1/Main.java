package com.lab1;

public class Main {
    public static void main(String[] args) {
        MyThread t1 = new MyThread("t-1");
        MyThread t2 = new MyThread("t-2");
        MyThread t3 = new MyThread("t-3");
        MyThread t4 = new MyThread("t-4");
        MyThread t5 = new MyThread("t-5");
        MyThread t6 = new MyThread("t-6");
        MyThread t7 = new MyThread("t-7");

        try {
            t1.start();
            t2.start();
            t3.start();

            t1.join();
            t2.join();
            t3.join();

            System.out.println("\n\n");

            t4.start();
            t4.join();

            System.out.println("\n\n");

            t5.start();
            t6.start();
            t7.start();

            t5.join();
            t6.join();
            t7.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
