package com.lab1;

public class MyThread extends Thread {
    private final String name;

    MyThread(String name) {
        this.name = name;
    }

    public void run() {
        try {
            System.out.println("Thread-ul `" + name + "` ruleaza");

            for (int i = 1; i <= 2; i++) {
                System.out.println("Thread `" + name + "` iteration " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Executarea thread-ului `" + name + "` a luat sfarsit");
    }
}
